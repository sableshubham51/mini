#include<stdio.h>

#include<string.h>
#include "library.h"



//user Function

void user_accept(user_t *u)                            
{
    printf("\nEnter ID:\n");
    scanf("%d",&u->id);
    u->id=get_max_user_id();
    printf("\nEnter Name :\n");
    scanf("%s",u->name);
    printf("\nEnter Email:\n");
    scanf("%s",u->email);
    printf("\nEnter Password:\n");
    scanf("%s",u->pass);
    printf("\nEnter Phone:\n");
    scanf("%s",u->phone);
    strcpy(u->role,ROLE_MEMBER);
}
void user_accept_without_get_id(user_t *u)
{
    printf("\nEnter Name :\n");
    scanf("%s",u->name);
  //  printf("\nEnter Email:\n");
   // scanf("%s",u->email);
   // printf("\nEnter Password:\n");
   // scanf("%s",u->pass);
    printf("\nEnter Phone:\n");
    scanf("%s",u->phone);
   // strcpy(u->role,ROLE_MEMBER);
}





void user_display(user_t *u)
{
    printf("Id:%d Name :%s email: %s phone :%s role :%s\n",u->id,u->name,u->email,u->phone,u->role);
}



int get_max_user_id()
{
    FILE *fp;
    int size=sizeof(user_t);
    int max;
    user_t u;
    fp=fopen(USER_DB,"rb");
    if(fp==NULL)
    max=max+1;
    fseek(fp,-size,SEEK_END);
    if(fread(&u,size,1,fp))
    {
        max=u.id;
    }
    
    fclose(fp);
    return max+1;   
      
}



//book function
void book_accept(book_t *b)
{
   // printf("\nEnter Book ID :\n");
   // scanf("%d",&b->id);
    printf("\nEnter Book Name :");
    scanf("%s",b->name);
    printf("\nEnter Author :");
   // scanf("%*s");
    scanf("%s",b->author);
    printf("\nEnter Subject Name :");
    //scanf("%*s");
    scanf("%s",b->subject);
    printf("\nEnter Book Price :");
    //scanf("%*s");
    scanf("%lf",&b->price);
    printf("\nEnter Book ISBN :");
    scanf("%s",b->isbn);

    /*printf("Book name: ");
	scanf("%s",b->name);
	printf("\nAuthor Name: ");
    scanf("%*s");
	scanf("%s",b->author);
	printf("\nSubject Name: ");
	scanf("%s",b->subject);
	printf("\nBook Price: ");
	scanf("%lf",&b->price);
	printf("\nISBN Code: ");
	scanf("%s",b->isbn);*/
}
void book_display(book_t *b)
{
    printf("BookId :%d  Book Name:%s  Author :%s  Subject : %s  Price : %lf  ISBN code :%s\n",b->id,b->name,b->author,b->subject,b->price,b->isbn);
}
void bookcopy_accept(bookcopy_t *b)
{
    //printf("\nEnter Id :\n");
    //scanf("%d",&b->id);
    printf("Book Id :\n");
    scanf("%d",&b->bookid);
    printf("Enter Rack :\n");
    scanf("%d",&b->rack);
    printf("Book Status :\n");
   // scanf("%s",STATUS_AVAILABLE);
    strcpy(b->status,STATUS_AVAILABLE);
}
void bookcopy_display(bookcopy_t *b)
{
    printf("\nID: %d  Book Id: %d Rack: %d Status: %s \n",b->id,b->bookid,b->rack,b->status);
}


int get_max_book_id()
{
    
    FILE *fp;
    int size=sizeof(book_t);
    int max=0;
    book_t b;
    fp=fopen(BOOK_DB,"ab");
    if(fp==NULL)
    max=max+1;
    fseek(fp,-size,SEEK_END);
    if(fread(&b,size,1,fp))
    {
        max=b.id;
    }
    
    fclose(fp);
    return (max+1);   
}

int get_next_bookcopy_id()
{
    FILE *fp;
    int max=0;
    int size=sizeof(bookcopy_t);
    bookcopy_t b;
    fp=fopen(BOOKCOPY_DB,"rb");
    if(fp==NULL)
    return max+1;
    fseek(fp,-size,SEEK_END);
    if(fread(&b,size,1,fp)>0)
    {
        max=b.id;
    }
    fclose(fp);
    return (max+1);
}









//8/oct/2020 code
void issuerecord_accept(issuerecord_t *r) {
	printf("\nissue id: \n");
	scanf("%d", &r->id);
	printf("\ncopy id: \n");
	scanf("\n%d\n", &r->copyid);
	printf("\nmember id: \n");
	scanf("%d", &r->memberid);

	printf("\nissue date :\n");
    date_accept(&r->issue_date);
   // r->issue_date=
	date_accept(&r->issue_date);
   // r->issue_date=date_current();
	r->return_duedate = date_add(r->issue_date, BOOK_RETURN_DAYS);
	memset(&r->return_date, 0, sizeof(date_t));
	r->fine_amount = 0.0;
}

void issuerecord_display(issuerecord_t *r) {
	printf("issue record: %d copy Id: %d  member Id: %d  find: %.2lf :%s\n", r->id, r->copyid, r->memberid, r->fine_amount);
	printf("issue ");
	date_print(&r->issue_date);
	printf("return due ");
	date_print(&r->return_duedate);
	printf("return ");
	date_print(&r->return_date);
}




// payment functions
void payment_accept(payment_t *p) {
	printf("id: ");
	scanf("%d", &p->id);
	printf("member id: ");
	scanf("%d", &p->memberid);
	printf("type (fees/fine): ");
	scanf("%s", p->type);
	printf("amount: ");
	scanf("%d", &p->amount);
	p->tx_time = date_current();
	if(strcmp(p->type,PAYMENT_TYPE_FEES) == 0)
		p->next_pay_duedate = date_add(p->tx_time, MEMBERSHIP_MONTH_DAYS);
	else
		memset(&p->next_pay_duedate, 0, sizeof(date_t));
}

void payment_display(payment_t *p) {
	printf("payment: %d member: %d Payment Type %s, amount: %.2lf\n", p->id, p->memberid, p->type, p->amount);
	printf("payment ");
	date_print(&p->tx_time);
	printf("payment due");
	date_print(&p->next_pay_duedate);
}



void user_add(user_t *u)
 {
    FILE *fp;
    int found=0;
	// open the file for appending the data
    fp = fopen(USER_DB,"ab");
// member functions
    if(fp==NULL)
    {
        perror("\nFile Doesn't Exits..!!\n");
        return;
    }
    fwrite(u,sizeof(user_t),1,fp); 
    printf("\nUser Added Successfully..\n");
    fclose(fp);
}
int find_user_by_email(user_t *u,char email[])
{
     FILE *fp;
     int found=0;
     fp=fopen(USER_DB,"rb");
     if(fp==NULL)
     {
         perror("User Email Not Found In Database..!!");
         return found;
     }
    while(fread(u,sizeof(user_t),1,fp)>0)
    {
        if (strcmp(u->email,email)==0)
        {
            found=1;
            break;
        }
    }
     fclose(fp);
     return found;
}


void Book_find_by_name(char name[])
{
    FILE *fp;
     int found=0;
     book_t b;
     fp=fopen(BOOK_DB,"rb");
     if(fp==NULL)
     {
         perror("Book Doestn't Found in Database..!!");
         //return found;
     }
    while(fread(&b,sizeof(book_t),1,fp)>0)
    {
        if (strstr(b.name,name)!=NULL)
        {
            found=1;
            book_display(&b);
        }
    }
     fclose(fp);
     if(found==0)
     {
         printf("\nBook Not Match..!!\n");
     }
}



// bookcopy functions
void bookcopy_accept(bookcopy_t *b);
void bookcopy_display(bookcopy_t *b);



// issuerecord functions
void issuerecord_accept(issuerecord_t *r);
void issuerecord_display(issuerecord_t *r);

// payment functions
void payment_accept(payment_t *p);
void payment_display(payment_t *p);

// common functions
void sign_in();
void sign_up();
//void Edit_profile(user_t *u)
//{
    /*FILE *fp;
    //user_t u;
    char email[30],password[10];
    int invalid_user=0;
    int size=sizeof(user_t);
    fp=fp=fopen(USER_DB,"ab+");
    if(fp==NULL)
    {
        perror("File Doesn't Exits..!!\n");
        exit(1);
    }
    printf("\nEnter the Email\n");
    scanf("%s",email);
    printf("\nEnter the Password\n");
    scanf("%s",passw  ord);
    if(find_user_by_email(&u,email)==1)
    {
        if (strcmp(password,u.pass)==0)
        {   
            invalid_user=1;
        }

    }
    while (fread(&u,size,1,fp)>0)
    {
        if (invalid_user==1)
        {
            break;
        }
        

    }
   if (invalid_user)
    {
        long size=sizeof(user_t);
        user_t nu;
        user_accept_without_get_id(&nu);
        nu.id=u.id;
        fseek(fp,-size,SEEK_CUR);
        fwrite(&nu,size,1,fp);
        printf(" Profile Updated...  \n");
    }else
    {
        printf("Enter Valid User And Password..!!\n");
    }  */ 
    
//}


/*void Change_Password(user_t *u,char password[])
{

    FILE *fp;
    char chngpassword[10];
    int found=0;
    int size=sizeof(user_t);
    fp=fopen(USER_DB,"ab+");
    if(fp==NULL)
    {
        perror("\nFile Doesn't Exits..!!\n");
    }
    while(fread(&u,sizeof(user_t),1,fp)>0)
    {
        if(strcmp(password,u->pass)==0)
        {
            found=1;
            break;
        }
    }
    printf("\nEnter New Password :\n");
    scanf("%s",chngpassword);
    if(found)
    {
        long size=sizeof(book_t);
        user_t nu;
     //   user_accept_without_get_id(&nu);
       // nu.id=u.id;
        strcpy(nu.pass,password);
        fseek(fp,-size,SEEK_CUR);
        fwrite(&nu,size,1,fp);
        printf(" Password Updated..  \n");
        printf("\n");
        user_display(&nu);
    }

}
*/



