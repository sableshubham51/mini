#ifndef _LIBRARY_H
#define _LIBRARY_H

#include "date.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define ROLE_OWNER         "owner"
#define ROLE_LIBRARIAN     "liabrarian"
#define ROLE_MEMBER        "user"
#define STATUS_AVAILABLE   "available"
#define STATUS_ISSUED      "issued"
#define PAYMENT_TYPE_FEES  "fees"
#define PAYMENT_TYPE_FINE  "fine"
#define FINE_PER_DAY          5
#define BOOK_RETURN_DAYS      7
#define MEMBERSHIP_MONTH_DAYS 30
#define EMAIL_OWNER         "sable123"



#define USER_DB             "user.db"
#define ISSUERECORDDB       "isudb"
#define BOOK_DB             "bookdb"
#define BOOKCOPY_DB         "bookcopydb"
#define PAYMENTS            "payntdb"






typedef struct user
{
    int id;
    char name[30];
    char email[30];
    char phone[30];
    char pass[10];
    char role[15];
    
}user_t;
typedef struct book
{
    int id;
    char name[40];
    char author[40];
    char subject[40];
    double  price;
    char isbn[16];
}book_t;
typedef struct bookcopy
{
    int id;
    int bookid;        
    int rack;
    char status[20];
}bookcopy_t;
typedef struct issuerecord
{
    int id;
	int copyid;
	int memberid;
	date_t issue_date;
	date_t return_duedate;
	date_t return_date;
	double fine_amount;

    
}issuerecord_t;
typedef struct payment
{
    int id;
    int memberid;
    double amount;
    char type[10];
    date_t tx_time;
    date_t next_pay_duedate;
}payment_t;






//user function
void user_accept();
void user_display();






//book function
void book_accept(book_t *u);
void book_display(book_t *u);                           
int get_max_book_id();





//owner function
void owner_area(user_t *u);
void Appoint_librarian();
void Edit_profile_Owner();



//member function
void member_area(user_t *u);
void Edit_profile_Member();
void Bookcopy_CheckAvailability_Member();





//librarian function
void librarian_area(user_t *u);
void Add_new_member();
void add_new_book();
int get_max_book_id();
void Find_Book();
void Book_Edit_by_id();
void Edit_profile_librarian();
void Bookcopy_add();
void Bookcopy_CheckAvailability_details();









//issuerecord
void issuerecord_accept(issuerecord_t *r);
void issuerecord_display(issuerecord_t *r);










//payments function
void payment_accept(payment_t *p);
void payment_display(payment_t *p);






//common Function
void sign_up();
void sign_in();
void Edit_profile(user_t *u);
void change_password(user_t *u);
void user_add(user_t *u);
int find_user_by_email(user_t *u,char email[]);
int get_max_user_id();
void Book_find_by_name(char name[]);
void user_accept_without_get_id(user_t *u);
//void Change_Password(user_t *u);
int get_next_bookcopy_id();
void bookcopy_accept(bookcopy_t *b);
void bookcopy_display(bookcopy_t *b);



//void Register_user();




#endif
