#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "library.h"


void librarian_area(user_t *u)
{
    int choice;
    do
    {
       printf("\n0.Sign Out\n1.Add New Member\n2.Edit Profile\n3.Change Password\n4.Find Book\n5.Edit Book\n6.Add New Book\n7.Check Avalability\n8.Add New Copy\n9.Change Rack\n10.Add New Rack\n11.Take Payment\n12.issue Copy\n13.Payment History\nEnter The Choice : ");
       scanf("%d",&choice);
       switch (choice)
       {
       case 1:
            Add_new_member();
           break;
       case 2:
            Edit_profile_librarian(); 
           break;
        case 3:
          // Change_Password();
           break;
       case 4:
            Find_Book();
           break;
        case 5:
            Book_Edit_by_id();
           break;
        case 6:
           add_new_book();
           break;
       case 7:
            Bookcopy_CheckAvailability_details();
           break;
        case 8:
           Bookcopy_add();
           break;
       case 9:
           break;
        case 10:
           
           break;
        case 11:
           
           break;
       case 12:
           break;
        case 13:
           
           break;
       
       default:
           break;
       }
    }while (choice!=0);
}


// Appoint Member function
void Add_new_member()
{
    user_t u;
    user_accept(&u);
    user_add(&u);
    strcpy(u.role,ROLE_MEMBER);
}

//Add New Book function
void add_new_book()
{
    FILE *fp;
    book_t b;
    book_accept(&b);
    b.id=get_max_book_id();
    fp=fopen(BOOK_DB,"ab");
    if(fp==NULL)
    {
        perror("File Doesn't Exits..!!\n");
        exit(1);
    }
    fwrite(&b,sizeof(book_t),1,fp);
    printf("\nBook Added Successfully..\n");
    fclose(fp); 
}
void Find_Book()
{
    char name[80];
    printf("\nEnter The Book That You Want To Search:");
    scanf("%s",name);
    Book_find_by_name(name);

}
/*void book_Edit(char name[])
{
    FILE *fp;
    book_t b;
    int size=sizeof(book_t);
    fp=fopen(BOOK_DB,"ab+");
    if(fp==NULL)
    {
        perror("\nFile Does't Exits..!!\n");
    }
    fseek(fp,-size,SEEK_END);
    while(fread(&b,size,1,fp)>0)
    {

    }
*/
void Book_Edit_by_id()
{
    FILE *fp;
    int found=0,id;
    book_t b;
    int size=sizeof(book_t);
    printf("\nEnter the Book ID :");
    scanf("%d",&id);
    fp=fopen(BOOK_DB,"ab+");
    if(fp==NULL)
    {
        perror("File Doesn't Exits..!!");
        exit(1);
    }
    while (fread(&b,size,1,fp)>0)
    {
        if(id==b.id)
        {
            found=1;
        }

    }
    if(found==1)
    {
        long size=sizeof(book_t);
        book_t nb;
        book_accept(&nb);
        nb.id=b.id;
        fseek(fp,-size,SEEK_CUR);
        fwrite(&nb,size,1,fp);
        printf(" Book Updated..  \n");
    }
    else
    {
        printf("\nBook Doesn't Found..!!\n");
    }
}
void Edit_profile_librarian()
{
    FILE *fp;
    int found=0;
    char email[30],password[10];
    user_t u;
    int size=sizeof(user_t);
    printf("\nFor Varification : \n");
    printf("Enter the Email\n");
    scanf("%s",email);
    printf("\nEnter the Password\n");
    scanf("%s",password);
    if(find_user_by_email(&u,email)==1)
    {
        if (strcmp(password,u.pass)==0)
        {   
            found=1;
        }
    }
    fp=fopen(BOOK_DB,"ab+");
    if(fp==NULL)
    {
        perror("File Doesn't Exits..!!");
        exit(1);
    }
    while (fread(&u,size,1,fp)>0)
    {
        if(find_user_by_email(&u,email)==1)
        {
            found=1;
        }

    }
    if(found==1)
    {
        long size=sizeof(book_t);
        user_t nu;
        user_accept_without_get_id(&nu);
        nu.id=u.id;
        fseek(fp,-size,SEEK_CUR);
        fwrite(&nu,size,1,fp);
        printf(" profile Updated..  \n");
        printf("\n");
        user_display(&nu);
    }
    else
    {
        printf("\nPlease Enter Valid Email and Password..!!\n");
    }  
}


void Bookcopy_add()
{
    FILE *fp;
    bookcopy_t b;
    bookcopy_accept(&b);
    b.id=get_next_bookcopy_id();
    fp=fopen(BOOKCOPY_DB,"ab");
    if(fp==NULL)
    {
        perror("\nFile Doesn't Exits..!!\n");
        exit(1);
    }
    fwrite(&b,sizeof(bookcopy_t),1,fp);
    printf("\nBook Copy Added Into the File.\n");
    fclose(fp);
}


void Bookcopy_CheckAvailability_details()
{
    FILE *fp;
    int book_id;
    bookcopy_t b;
    //int found=0;
    int count=0;
    printf("\nEnter The Book ID :\n");
    scanf("%d",&book_id);
    fp=fopen(BOOKCOPY_DB,"rb");
    if(fp==NULL)
    {
        perror("File Doesn't Exits..!!\n");
        exit(1);
    }
    while (fread(&b,sizeof(bookcopy_t),1,fp)>0)
    {
        if(book_id==b.id && strcmp(b.status,STATUS_AVAILABLE)==0)
        {
           bookcopy_display(&b);
           count ++;
        }
    }
    fclose(fp);
    if(count==0)
    {
        printf("\nBook Copy Not Available..\n ");
    }   

}










